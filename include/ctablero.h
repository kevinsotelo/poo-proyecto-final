#ifndef CTABLERO_H
#define CTABLERO_H
#include "cui.h"

class CTABLERO
{
    private:
    CUI ui;
    int nivel=0;
    int vidas=0;
    int intentos=0;
    int fila=0;
    int columna=0;
    bool fin=false;
    int matriz[1][1];
    char *matrizVisible[1][1];

    public:
        CTABLERO();
        virtual ~CTABLERO();

        void Setnivel( int val) { nivel = val; }
        int Getnivel() { return nivel; }
        void Setvidas(int val){vidas=val;}
        int Getvidas(){return vidas;}
        void Setintentos(int val){intentos=val;}
        int Getintentos(){return intentos;}
        void Setfila(int val){fila=val;}
        int Getfila(){return fila;}
        void Setcolumna(int val){columna=val;}
        int Getcolumna(){return columna;}

        void tableroFacil();
        void tableroMedio();
        void tableroDificil();
        void pedirCasilla();

};

#endif // CTABLERO_H
