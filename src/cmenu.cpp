#include "cmenu.h"
#include <stdlib.h>

CMENU::CMENU()
{
    //ctor
}

CMENU::~CMENU()
{
    //dtor
}
void CMENU::mostrarOpciones(){
    ui.mostrarMensaje("\n\t\t - B U S C A M I N A S - \n\n");
    ui.mostrarMensaje("   Selecciona el nivel de juego\n\n");
    ui.mostrarMensaje("   Nivel             Escribe\n\n");
    ui.mostrarMensaje("   Facil             1\n");
    ui.mostrarMensaje("   Medio             2\n");
    ui.mostrarMensaje("   Dificil           3\n");
}
void CMENU::pedirOpcion(){
    do{
        ui.mostrarMensaje("\n   Ingresa la dificultad: ");
        Setopcion(ui.recibirDato());
        if(opcion<1 || opcion>3){
            ui.mostrarMensaje("\nERROR: INGRESE UN VALOR ENTRE 1 Y 3");
            }
            }while(opcion<1 || opcion>3);
            system("cls");
}
void CMENU::realizarOperacion(){
    switch(opcion){
    case 1:
        tablero.tableroFacil();
        break;
    case 2:
        tablero.tableroMedio();
        break;
    case 3:
        tablero.tableroDificil();
        break;
    }
}
     void CMENU::run(){
     mostrarOpciones();
     pedirOpcion();
     realizarOperacion();
     }
