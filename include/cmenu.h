#ifndef CMENU_H
#define CMENU_H
#include "cui.h"
#include "ctablero.h"

class CMENU
{
    private:
        CUI ui;
        CTABLERO tablero;
        int opcion;

    public:
        CMENU();
        virtual ~CMENU();

        void Setopcion(int val) { opcion = val; }
        int Getopcion() { return opcion; }

        void mostrarOpciones();
        void pedirOpcion();
        void realizarOperacion();

        void run();
};

#endif // CMENU_H
